import React, { Component } from "react";
import todosList from "./todos.json";
import TodoList from "./components/ToDoList";



class App extends Component {
  state = {
    todos: todosList,
  };
  
  handleToDo = (event) =>{
    if (event.key==="Enter")
    {
      let newToDo = { 
        "userId": 1,
        "id": Math.random()*5000,
        "title": event.target.value,
        "completed": false
      };
      let updatedToDos = [...this.state.todos, newToDo];
      this.setState({todos: updatedToDos});
    };
  };

  completedToDos = (event,id) => {
    let newToDos = this.state.todos.map(todo =>{
      if(todo.id === id){
        return {...todo, completed:!todo.completed};
     }

       return todo; 
       
     
    })
   this.setState({todos:newToDos})
 };

 deleteToDos = (event,id) =>{
  let newToDos = this.state.todos.filter(todo => todo.id !== id)
     this.setState({todos:newToDos})
 };

 clearTodo = event => {
   let newToDos = this.state.todos.filter(todo =>todo.completed === false)
   this.setState({todos:newToDos})
 };

  
  
  

  
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" autofocus onKeyPress={this.handleToDo} />
        </header>
        <TodoList todos={this.state.todos} completedToDos={this.completedToDos} deleteToDos={this.deleteToDos} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearTodo}>Clear completed</button>
        </footer>
      </section>
    );
  }
}






export default App;
