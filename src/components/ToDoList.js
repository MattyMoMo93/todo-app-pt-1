import React, {Component} from 'react'
import TodoItem from "./ToDoItems";

class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map((todo) => (
              <TodoItem title={todo.title} completed={todo.completed} completedToDos={this.props.completedToDos} id={todo.id}  deleteToDos={this.props.deleteToDos} />  
            ))}
          </ul>
        </section>
      );
    }
  }

  export default TodoList;